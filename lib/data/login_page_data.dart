import 'package:dio/dio.dart';

class LoginPageData{
  String email;
  String password;
  bool rememberMe = true;
  FormData getFormDataValue(){
    return FormData.from({
      "email":email,
      "password":password,
    });
  }
}

class RegistrationPageData{
  String firstName;
  String lastName;
  String email;
  String password;
  FormData getFormDataValue(){
    return FormData.from({
      'email': email,
      'password': password,
      'firstName': firstName,
      'lastName': lastName,
    });
  }
}

class RessetPasswordData{
  String email;
  FormData getFormDataValue(){
    return FormData.from({
      'email': email,
    });
  }
}