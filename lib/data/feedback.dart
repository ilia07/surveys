import 'package:dio/dio.dart';

class FeedbackData{
  String model;
  String file = 'null';
  FormData getFormDataValue(){
    return FormData.from({
      "model": model,
      "file": file,
    });
  }
}