import 'package:surveys/core/utils/date_time.dart';

class SurveysList{
  final int totalCount;
  final List<Survey> surveys;
  SurveysList ({this.totalCount, this.surveys});

  factory SurveysList.fromJson(Map<String, dynamic> json){
    var list = json['elements'] as List;
    List<Survey> surveyList = list.map((i)=>Survey.fromJson(i)).toList();
    return SurveysList(
      totalCount: json['totalCount'],
      surveys: surveyList,
    );
  }
}

class Survey{
  String id;
  String date;
  String title;
  Creator creator;
  Survey({this.creator,this.date,this.id,this.title});
  factory Survey.fromJson(Map<String,dynamic> json){
    return Survey(
      id: json['id'],
      date: convertDate(json['createdAt']),
      title: json['title'],
      creator : Creator.fromJson(json['createdBy']),
    );
  }

}

class Creator{
  String email;
  String firstName;
  String lastName;
  Creator({this.email,this.firstName,this.lastName});
  factory Creator.fromJson(Map<String , dynamic> json){
    return Creator(
      email: json['email'],
      firstName: json['firstName'],
      lastName: json['lastName'],
    );
  }
}

class SurveyFull{
  String id;
  String date;
  String title;
  Creator creator;
  final List <Question> qustions;
  SurveyFull({this.date,this.creator,this.title,this.id,this.qustions});
  factory SurveyFull.fromJson(Map<String , dynamic> json){
    var list = json['questions'] as List;
    List<Question> questionList = list.map((i) => Question.fromJson(i)).toList();
    return SurveyFull(
      id: json['id'],
      date: convertDate(json['createdAt']),
      title: json['title'],
      creator : Creator.fromJson(json['createdBy']),
      qustions: questionList,
    );
  }
}

class Question{
  String id;
  String title;
  int maxAnswers;
  String type;
  bool isField;
  String fieldTitle;
  bool isComment;
  int fieldCount;
  final List<Answer> answers;
  Question({this.id, this.title ,this.answers  ,this.fieldCount ,this.fieldTitle ,
  this.isComment ,this.isField ,this.maxAnswers , this.type});
  factory Question.fromJson(Map <String,dynamic> json) {
    var list = json['answers'] as List;
    if(json['type'] == 'Opened'){
      print('opened');
      return Question(
        id: json['id'],
        maxAnswers: json['maxAnswers'],
        type: json['type'],
        isField: json['isField'],
        fieldTitle: json['fieldTitle'],
        fieldCount: json['filedCount'],
        isComment: json['isComment'],
        title: json['title'],
      );
    }else{
      List<Answer> answersList = list.map((i) => Answer.fromJson(i)).toList();
      return Question(
        id: json['id'],
        maxAnswers: json['maxAnswers'],
        type: json['type'],
        isField: json['isField'],
        fieldTitle: json['fieldTitle'],
        fieldCount: json['filedCount'],
        isComment: json['isComment'],
        title: json['title'],
        answers: answersList,
      );
    }
  }
}

class Answer{
  String id;
  String title;
  Answer({this.title,this.id});
  factory Answer.fromJson(Map<String,dynamic> json){
    return Answer(
      id: json['id'],
      title: json['title'],
    );
  }
}
