import 'package:dio/dio.dart';

class User{
  String id;
  String firstName;
  String lastName;
  String email;
  String picture;
  int createdSurveys;
  int createdForms;
  User({this.firstName,this.lastName,this.email,this.picture, this.createdForms,this.createdSurveys,this.id});
  factory User.fromJson(Map<String, dynamic> json){
    return User(
      id: json['id'],
      email: json['email'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      picture: json['picture'],
      createdForms: json['createdForms'],
      createdSurveys: json['createdSurveys'],
    );
  }
}

class SetData{
  String model;
  FormData getFormDataValue(){
    return FormData.from({
      'model': model,
    });
  }
}

class Password{
  String oldPassword;
  String newPassword;
  String newPasswordClone;
  FormData getFormDataValue(){
    return FormData.from({
    });
  }
}