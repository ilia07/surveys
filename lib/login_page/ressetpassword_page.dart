import 'package:flutter/material.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/data/login_page_data.dart';
import 'package:surveys/home_page/home_page.dart';
import 'package:surveys/core/AppConfig.dart';
import 'package:http/http.dart' as http;
import 'package:surveys/core/utils/validate.dart';


class RessetPasswordPage extends StatefulWidget {
  static String tag = 'resset-page';
  @override
  _RessetPasswordPageState createState() => _RessetPasswordPageState();
}

class _RessetPasswordPageState extends State<RessetPasswordPage> {
  AppConfig _appConfig = AppConfig();
  final _formKey = GlobalKey<FormState>();
  RessetPasswordData data = RessetPasswordData();
  bool notFindUser = false;

void sendForm() async{
    _formKey.currentState.save();
    try{
      String apiRoot = await _appConfig.getApiRootUrl();
      var response = await http.get('$apiRoot/v1/account/forgotPassword/$data.email');
      print(response.body);
      if(response.statusCode == 404){
        setState(() {
          notFindUser = true;
        });
      }
      if(response.statusCode == 200){
        setState(() {
          Navigator.of(context).pushNamed(HomePage.tag);
        });
      }  
    }catch(e){
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget ressetPassword(){
      return 
      Form(
        key:_formKey,
        child:
          Column(
            children: <Widget>[
              Hero(
                tag: 'hero',
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 48.0,
                  child: Image.asset('assets/logo.png'),
                ),
              ),
              Text('Surveys'.toUpperCase(),style:TextStyle(color: Colors.teal,fontSize: 24, fontWeight: FontWeight.w600)),
              SizedBox(height: 20),
              Text('Сброс пароля', style:TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
              SizedBox(height: 20),
              Text('На вашу почту будет отправлено предложение о смене пароля',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14,color: Colors.black54, fontWeight: FontWeight.w400),),
              SizedBox(height: 10),
              TextFormField(
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                validator: (String value){
                  return validateEmail(value);
                },
                onSaved:(String value){
                  setState((){
                    data.email = value;
                  });
                },
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                  labelStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.black54),
                  labelText: 'Введите Email',
                  contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                  suffixIcon: 
                    Padding(
                      padding: new EdgeInsets.symmetric(vertical: 0.0),
                      child: Icon(Icons.mail_outline, color: Colors.black45)
                    ),
                ),
              ),
              SizedBox(height: 24.0),
              Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width*0.5,
                  child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  onPressed: () {
                    if(_formKey.currentState.validate()){
                      sendForm();
                    }
                  },
                  color: primaryColor,
                  child: Text('Отправить', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
                ),
              ),
              Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width*0.5,
                  child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(HomePage.tag);
                  },
                  color: Colors.red,
                  child: Text('Отмена', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
                ),
              ),
              notFindUser? 
              Container(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Text('Пользватель с таким email не наёден', style:TextStyle(color: Colors.red, fontSize: 14,fontWeight: FontWeight.w400)),
              ):Container(),
            ],
          )
        );
    }

    return Scaffold(backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 12.0, right: 12.0),
          children: <Widget>[
            ressetPassword(),
          ],
        ),
      ),
    );
  }
}