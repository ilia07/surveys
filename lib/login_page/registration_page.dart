import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:surveys/data/login_page_data.dart';
import 'package:surveys/login_page/success-page.dart';
import 'package:surveys/home_page/home_page.dart';
import 'package:surveys/core/AppConfig.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:surveys/core/utils/validate.dart';
import 'package:surveys/Theme.dart';


class RegistrationPage extends StatefulWidget {
  static String tag = 'registration-page';
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {

  final _formKey = GlobalKey<FormState>();
  RegistrationPageData data = RegistrationPageData();
  AppConfig _appConfig = AppConfig();
  var dio = Dio();
  FocusNode firstNameFocus = FocusNode();
  FocusNode lastNameFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  FocusNode clonePasswordFocus = FocusNode();

  void sendForm() async{
    _formKey.currentState.save();
    try{
      String apiRoot = await _appConfig.getApiRootUrl();
      var body = jsonEncode({'email': data.email, 'password': data.password, 'firstName': data.firstName, 'lastName': data.lastName});
      var response = await http.post('$apiRoot/v1/account', headers: {"Content-Type": "application/json"}, body: body);
      if(response.statusCode == 200){
        setState(() {
         Navigator.of(context).pushNamed(SuccessPage.tag); 
        });
        if(response.statusCode == 400){
          
        }
      }  
    }catch(e){
    }
  }

  String _password;
  String _passwordClone;

  @override
  Widget build(BuildContext context) {
    Widget formInput(){
      return Form(
        key: _formKey,
        child:
          Column(
          children: <Widget>[
            Hero(
                tag: 'hero',
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 48.0,
                  child: Image.asset('assets/logo.png'),
                ),
            ),
            SizedBox(height: 10,),
            Text('Surveys'.toUpperCase(),style:TextStyle(color: primaryColor ,fontSize: 24.0, fontWeight: FontWeight.w600)),
            SizedBox(height: 10.0),
            Text('Регистрация', style:TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
            SizedBox(height: 10.0),
            TextFormField(
              keyboardType: TextInputType.text,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              autofocus: false,
              onFieldSubmitted: (value){
                setState(() {
                 firstNameFocus.unfocus();
                 FocusScope.of(context).requestFocus(lastNameFocus);
                });
              },
              validator: (val){
               return validateName(val);
              },
              onSaved:(String value){
                data.firstName = value;
              },
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                labelText: 'Имя',
                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              ),
            ),
            SizedBox(height: 10.0),
            TextFormField(
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              autofocus: false,
              focusNode: lastNameFocus,
              validator: (val){
                return validateName(val);
              },
              onFieldSubmitted: (value){
                setState(() {
                 firstNameFocus.unfocus();
                 FocusScope.of(context).requestFocus(emailFocus);
                });
              },
              onSaved:(String value){
                data.lastName = value;
              },
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                labelText: 'Фамилия',
                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              ),
            ),
            SizedBox(height: 10.0),
            TextFormField(
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              autofocus: false,
              focusNode: emailFocus,
              onFieldSubmitted: (value){
                setState(() {
                 firstNameFocus.unfocus();
                 FocusScope.of(context).requestFocus(passwordFocus);
                });
              },
              validator: (value){
               return validateEmail(value);
              },
              onSaved:(String value){
                data.email = value;
              },
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.8))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                labelText: 'E-mail',
                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              ),
            ),
            SizedBox(height: 10.0),
            TextFormField(
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              autofocus: false,
              focusNode: passwordFocus,
              onFieldSubmitted: (value){
                setState(() {
                 firstNameFocus.unfocus();
                 FocusScope.of(context).requestFocus(clonePasswordFocus);
                });
              },
              obscureText: true,
              validator: (value){
                return validatePassword(value);
              },
              onSaved:(String value){
                data.password = value;
                setState(() {
                  _password = value;
                });
              },
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                labelText: 'Пароль',
                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              ),
            ),
            SizedBox(height: 10.0),
            TextFormField(
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              autofocus: false,
              obscureText: true,
              focusNode: clonePasswordFocus,
              onFieldSubmitted: (value){
                setState((){
                  clonePasswordFocus.unfocus();
                });
              },
              validator: (value){
                  return validatePassword(value);
              },
              onSaved:(String value){
                _passwordClone = value;
              },
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                labelText: 'Пароль ещё раз',
                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              ),
            ),
            SizedBox(height: 14.0),
            Container(
                width: 200,
                height: 50,
                child: RaisedButton(
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                onPressed: () {
                  if(_formKey.currentState.validate()){
                    if(_password == _passwordClone){
                      sendForm();
                    }else{
                      Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text('Пароли не совпадают', style: TextStyle(color: Colors.red,fontSize: 14.0, fontWeight: FontWeight.w400)),
                      ));
                    }
                  }
                },
                color: primaryColor,
                child: Text('Подтвердить', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
              ),
            ),
            SizedBox(height: 10,),
            Container(
                height: 50,
                width: MediaQuery.of(context).size.width*0.5,
                child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop(HomePage.tag);
                },
                color: Colors.red,
                child: Text('Отмена', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
              ),
            ),
          ],
        )
      );
    }
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          children: <Widget>[
            formInput(),
          ],
        ),
      ),
    );
  }
}