import 'package:flutter/material.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/login_page/login_page.dart';


class SuccessPage extends StatefulWidget {
  static String tag = 'success-page';
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          children: <Widget>[
            Column(children: <Widget>[
              Hero(
              tag: 'hero',
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 48.0,
                child: Image.asset('assets/logo.png'),
              ),
            ),
            Text('Surveys'.toUpperCase(),style:TextStyle(color: Colors.teal,fontSize: 24.0, fontWeight: FontWeight.w600)),
            SizedBox(height: 20.0),
            Text('Спасибо за регистрацию в нашем приложении, на вашу почту отправлено сообщение для подтвержения регистрации.',
              style: TextStyle(color:Colors.black,fontSize: 14.0, fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
            SizedBox(height: 10.0),
            Container(
              height: 45,
              width: MediaQuery.of(context).size.width*0.5,
              child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(LoginPage.tag);
              },
              color: primaryColor,
              child: Text('Завершить', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
              ),
            ),
          ],)
        ],),
      ),
      
    );
  }
}