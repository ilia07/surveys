import 'package:flutter/material.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/login_page/login_page.dart';
import 'package:surveys/login_page/registration_page.dart';



class MainLoginPage extends StatefulWidget {
  static String tag = 'main-login-page';
  @override
  _MainLoginPageState createState() => _MainLoginPageState();
}

class _MainLoginPageState extends State<MainLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 12.0, right: 12.0),
          children: <Widget>[
            Hero(
              tag: 'hero',
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 48.0,
                child: Image.asset('assets/logo.png'),
              ),
            ),
            SizedBox(height: 10,),
            Center(
              child: Column(
                children: <Widget>[
                  Text('Surveys'.toUpperCase(),style:TextStyle(color: primaryColor,fontSize: 24, fontWeight: FontWeight.w600)),
                  SizedBox(height: 10),
                  Text('Приложение для прохождения опросов',style:TextStyle(color:Colors.black38,fontSize: 14, fontWeight: FontWeight.w300)),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width*0.5,
                      height: 45,
                      child: RaisedButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(LoginPage.tag);
                      },
                      color: primaryColor,
                      child: Text('Вход', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600)),
                    ),
                    ) 
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width*0.5,
                      height: 45,
                      child: RaisedButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        side: BorderSide(color: primaryColor, width: 2)
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(RegistrationPage.tag);
                      },
                      color: Colors.white,
                      child: Text('Регистрация', style: TextStyle(color: primaryColor,fontSize: 14,fontWeight: FontWeight.w600)),
                    ),
                    ) 
                  ),
              ],),)
          ],
        ),
      ),
    );
  }
}