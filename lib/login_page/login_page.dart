import 'package:flutter/material.dart';
import 'package:surveys/core/ui/LoadingProgressIndicator.dart';
import 'package:surveys/home_page/home_page.dart';
import 'package:surveys/login_page/registration_page.dart';
import 'package:surveys/login_page/ressetpassword_page.dart';
import 'package:dio/dio.dart';
import 'package:surveys/data/login_page_data.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/core/AppConfig.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveys/core/utils/validate.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}
class _LoginPageState extends State<LoginPage> {
  String email; String password;
  AppConfig _appConfig = AppConfig();
  final _formKey = GlobalKey<FormState>();
  LoginPageData data = LoginPageData();
  String token;
  FocusNode emailFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  
  var dio = Dio();
  bool sending = false;
  bool notValidData = false;
  bool loading = false;

void getToken() async{
  final prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');
  print(token);
  setState((){
    loading = true;
    if(token == null || token == ''){
    }else{
      Navigator.of(context).pushNamed(HomePage.tag);
    } 
  });
}

sendForm() async{
  final prefs = await SharedPreferences.getInstance();
    notValidData = false;
    _formKey.currentState.save();
    setState(() {
         sending = true; 
        });
    try{
      String apiRoot = await _appConfig.getApiRootUrl();
      var body = jsonEncode({'email': data.email, 'password': data.password});
      var response = await http.post('$apiRoot/v1/token', headers: {"Content-Type": "application/json"}, body: body);
      if(response.statusCode == 400){
        setState(() {
          notValidData = true; 
        });
      }
      if(response.statusCode == 200){
        setState(() {
          Navigator.of(context).pushNamed(HomePage.tag);
          var body = jsonDecode(response.body);
          var token = body['token'];
          prefs.setString('token', token);
          sending = false; 
        });
      }  
    }catch(e){
      
    }
  }

  Widget formInput(){
    return
      Form(
        key: _formKey,
      child: 
      Column(
        children: <Widget>[
          Hero(
            tag: 'hero',
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 48.0,
              child: Image.asset('assets/logo.png'),
            ),
          ),
          SizedBox(height: 10,),
          Text('Surveys'.toUpperCase(),style:TextStyle(color: Colors.teal,fontSize: 24.0, fontWeight: FontWeight.w600)),
          SizedBox(height: 20.0),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            focusNode: emailFocus,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            validator: (val) {
              return validateEmail(val);
            },
            onSaved: (val){
              setState(() {
                data.email = val; 
              });
            },
            onFieldSubmitted: (value){
              setState(() {
               emailFocus.unfocus();
               FocusScope.of(context).requestFocus(passwordFocus);
              });
            },
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
              labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
              labelText: 'Введите E-mail',
              suffixIcon: 
                Padding(
                  padding: new EdgeInsets.symmetric(vertical: 0.0),
                  child: Icon(Icons.email, color: Colors.black45)
                ),
              contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
            ),
          ),
          SizedBox(height: 20.0),
          TextFormField(
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            autofocus: false,
            obscureText: true,
            focusNode: passwordFocus,
            validator: (val){
              return validatePassword(val);
            },
            onSaved:(String val){
              setState((){
                data.password = val;
              });
            },
            onFieldSubmitted: (value){
              setState(() {
               passwordFocus.unfocus(); 
              });
            },
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
              labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
              labelText: 'Введите пароль',
              contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              suffixIcon: 
              Padding(
                padding: new EdgeInsets.symmetric(vertical: 0.0),
                child: Icon(Icons.lock, color: Colors.black45)
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Container(
              width: MediaQuery.of(context).size.width*0.5,
              height: 45,
              child: RaisedButton(
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              onPressed: () {
                if(_formKey.currentState.validate()){
                  sendForm();
                  setState(() {
                    
                  });
                }
              },
              color: primaryColor,
              child: Text('Войти', style: TextStyle(color: Colors.white,fontSize: 14.0, fontWeight: FontWeight.w600)),
            ),
          ),
          SizedBox(height: 10),
          notValidData? Text('Не верный логин или пароль',style: TextStyle(color: Colors.red, fontSize: 14, fontWeight: FontWeight.w400),) : Container(),
          Container(
            height: 30.0,
            child: 
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text('Забыли пароль?',style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),),
                FlatButton(
                child: Text(
                  'Сброс пароля',
                  style: TextStyle(color: Colors.teal, fontSize: 14, fontWeight: FontWeight.w300),
                ),
                onPressed: () {
                  setState(() {
                    Navigator.of(context).pushNamed(RessetPasswordPage.tag);
                  });
                },
              ),
            ],),
          ),
          Container(
            height: 30.0,
            child: 
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text('Ещё не зарегистрированы?', style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w300),),
                FlatButton(
                child: Text(
                  'Регистрация',
                  style: TextStyle(color: primaryColor, fontSize: 14, fontWeight: FontWeight.w300),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(RegistrationPage.tag);
                },
              ),
            ],),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    getToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading? 
    Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 30),
          children: <Widget>[
            formInput(),
          ],
        ),
      ),
    )
    : LoadingProgressIndicator();
  }
}