import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';

class AppConfig{
  static BaseOptions get dioOptions => BaseOptions(
    baseUrl: "",
    connectTimeout: 3000,
    receiveTimeout: 6000,
  );
  static const String API_ROOT = "http://surveys-project.xyz/api";
  static const String CONFIG_PATH = "assets/config.json";

  Map<String, dynamic> _json;
  String _jsonString;

  Future<Map<String, dynamic>> loadConfig() async {
    this._jsonString = await rootBundle.loadString(CONFIG_PATH);
    this._json = jsonDecode(this._jsonString);
    return this._json;
  }
  
  Future<String> getApiRootUrl() async => API_ROOT;
}