import 'package:flutter/material.dart';
import 'package:surveys/Theme.dart';

class LoadingProgressIndicator extends StatefulWidget {
  @override
  LoadingProgressIndicatorState createState() {
    return new LoadingProgressIndicatorState();
  }
}

class LoadingProgressIndicatorState extends State<LoadingProgressIndicator> {
  bool _textIsVisible = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails td) {
        setState(() {
          _textIsVisible = true;
        });
      },
      onTapUp: (TapUpDetails tu) {
        setState(() {
          _textIsVisible = false;
        });
      },
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CircularProgressIndicator(
              backgroundColor: Colors.black12,
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
            SizedBox(height: 8),
            AnimatedOpacity(opacity: _textIsVisible ? 1 : 0, child: Text('Загрузка', style: TextStyle(color: Color(0xff4bb9b5), fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.8)), duration: Duration(milliseconds: 300),),
          ],
        )
      ),
    );
  }
}