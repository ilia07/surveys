import 'package:flutter/material.dart';

void showSnackbar(BuildContext context, String text, {Color colorText = Colors.white, Color colorBackGround = Colors.blueAccent}) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: Text(text, style: TextStyle(color: colorText, fontWeight: FontWeight.w400)),
      backgroundColor: colorBackGround,
      duration: Duration(seconds: 1),
    )
  );
}
