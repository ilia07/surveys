String convertDate(String date){
  List<String> _temp = [];
  _temp = date.substring(0,10).replaceAll('-', '.').split('.');
  return _temp.reversed.join('.').toString();
}