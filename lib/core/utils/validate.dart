 String validateEmail(String value) {
    if (value.isEmpty) {
      return "Пожалуйста введите email";
    }
      String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
      "\\@" +
      "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
      "(" +
      "\\." +
      "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
      ")+";
    RegExp regExp = new RegExp(p);
    if (regExp.hasMatch(value)) {
      return null;
    }
    return 'Неправильный email';
  }
  
  String validateName(String value){
    if(value.isEmpty){
      return "Введите данные";
    }
    String p = "[А-Я]{1}"+"[а-я]{1,30}";
    RegExp regExp = new RegExp(p);
    if(regExp.hasMatch(value)){
      return null;
    }
    return 'Только русские буквы и первая буква должна быть большая';
  }

  String validatePassword(String value){
    if (value.isEmpty){
      return 'Пожалуйста введите пароль';
    }
    if(value.length< 8){
      return 'Слишком короткий пароль';
    }
    return null;
  }

  String validateEmpty(String value){
    if(value.isEmpty){
      return 'Заполните поле';
    }
    return null;
  }

  String validateFeedbackText(String value){
    if(validateEmpty(value) == null ){
      if(value.length < 10 || value.length > 100){
        return 'Количество символов от 10 до 1000.';
      }
      return null;
    }else{
      return validateEmpty(value);
    }
  }