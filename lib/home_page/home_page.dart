import 'package:flutter/material.dart';
import 'package:surveys/home_page/views/aboutapp.dart';
import 'package:surveys/home_page/views/profile.dart';
import 'package:surveys/home_page/views/main_page_surveys.dart';
import 'package:surveys/Theme.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{

  String pageTitle = 'Опросы';
  int _selectedIndex = 1;
  final _widgetsViews =[
    Profile(),
    Surveys(),
    AboutAppPage(),
  ];

  void setTitle(int index){
    switch (index) {
      case 0:
        pageTitle = 'Мой профиль';
        break;
      case 1:
        pageTitle = 'Опросы';
        break;
      case 2:
        pageTitle = 'О приложении';
        break;
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      setTitle(index);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(pageTitle,style:TextStyle(color:Colors.white,fontSize: 16, fontWeight: FontWeight.w600)),
        automaticallyImplyLeading: false,
        backgroundColor: primaryColor,
      ),
      body:_widgetsViews.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 20,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w400),
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Мой профиль')),
          BottomNavigationBarItem(icon: Icon(Icons.assignment), title: Text('Опросы')),
          BottomNavigationBarItem(icon: Icon(Icons.info_outline), title: Text('О приложении')),
        ],
        currentIndex: _selectedIndex,
        fixedColor: primaryColor,
        onTap: _onItemTapped,
      )
    );
  }
}