import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:surveys/data/profile_page_data.dart';
import 'package:surveys/Theme.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:surveys/core/AppConfig.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveys/core/ui/LoadingProgressIndicator.dart';
import 'package:surveys/core/utils/validate.dart';
import 'package:surveys/login_page/login_page.dart';
import 'package:surveys/login_page/main_login_page.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  User _user;
  final _formKeyProfile = GlobalKey<FormState>();
  final _formKeyPassword = GlobalKey<FormState>();
  SetData data = SetData();
  Password dataPassword = Password();
  var dio = Dio();
  AppConfig _appConfig = AppConfig();
  bool createProfile = false;
  String tempName;
  bool successSetPassword = false;
  bool error = false;

  void _getUserInformation() async{
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    var response = await http.get('$apiRoot/v1/account/info/', headers: {"Authorization": "Bearer $token"});
    if(response.statusCode == 200){
      setState((){
        _user = User.fromJson(json.decode(response.body));
      });
    }
    if(response.statusCode == 401){
      Navigator.of(context).pushNamed(LoginPage.tag);
    }
  }

  void deleteToken() async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');
  }

  void setUserInformation() async{
    _formKeyProfile.currentState.save();
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    try{
      data.model = jsonEncode({'firstName': _user.firstName, 'lastName': _user.lastName,'email': _user.email, 'pictureUrl': _user.picture});
      var response = await dio.post('$apiRoot/v1/account/info', options: Options(headers: {"Authorization":"Bearer $token"} ), data: data.getFormDataValue());
      if(response.statusCode == 200){
        setState((){
          var body = json.decode(response.data);
          var token = body['token'];
          prefs.setString('token', token);
        });
      }
      if(response.statusCode == 401){
      Navigator.of(context).pushNamed(LoginPage.tag);
      }
    }catch(e){
      print(e);
    }
  }

  void _setPassword() async{
    _formKeyPassword.currentState.save();
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    try{
      String body = json.encode({'password': dataPassword.oldPassword,'newPassword': dataPassword.newPassword,'confirmNewPassword': dataPassword.newPasswordClone });
      print(body);
      var response = await http.post('$apiRoot/v1/account/password', headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: body);
      if(response.statusCode == 200){
        setState((){
          successSetPassword = true;
          var body = json.decode(response.body);
          var token = body['token'];
          prefs.setString('token', token);
        });
      }
      if(response.statusCode == 400){
        setState(() {
         error = true; 
        });
      }
      if(response.statusCode == 401){
      Navigator.of(context).pushNamed(LoginPage.tag);
      }
    }catch(e){
      print(e);
    }
  }

  Widget _buildScreen(){
    if(_user != null){
      return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: <Widget>[
            Center(child: 
              Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  CircleAvatar(
                    radius: 35,
                    backgroundImage: NetworkImage(_user.picture),
                  ),
                  SizedBox(height: 12.0,),
                  Text(_user.firstName +' '+ _user.lastName, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black,fontSize: 14),),
                  SizedBox(height: 5.0),
                  Text(_user.email, style: TextStyle(color: Colors.black54,fontSize: 12.0, fontWeight: FontWeight.w400),),
                  SizedBox(height: 5.0),
                  Divider(color: primaryColor),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(width: 1.0),
                      Column(
                        children: <Widget>[
                          Text('Анкеты',style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w400),),
                          SizedBox(height: 2.0),
                          Text('${_user.createdForms}',style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),),
                        ],
                      ),
                      Container(
                        width: 0.0,
                        height: 40.0,
                        decoration: BoxDecoration(
                          border: Border.all(color: primaryColor,width: 0.3) ,
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Text('Опросы',style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w400),),
                          SizedBox(height: 2.0,),
                          Text('${_user.createdSurveys}',style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),),
                        ],
                      ),
                      SizedBox(width: 1.0),
                  ],),
                ],
              )
            ,),
            Divider(color: primaryColor),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width*0.5,
                  child: RaisedButton(
                    elevation: 0,
                    color: Colors.red,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: (){
                      setState(() {
                        deleteToken();
                        Navigator.pushReplacement(context,MaterialPageRoute(
                          builder: (context){
                            return MainLoginPage();
                          }
                        ));
                      });
                    },
                    child: 
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                      Text('Выход',style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600)),
                      SizedBox(width: 5,),
                      Icon(Icons.exit_to_app,size: 18,color: Colors.white,),
                    ],)
                  ),
                ),
              ],
            ),
            SizedBox(height: 20,),
            Center(
              child: Container(
                child: Form(
                  key:_formKeyProfile,
                  child: Column(
                    children: <Widget>[
                      Text('Редактирование персональных данных', style: TextStyle(fontSize: 16, color: Colors.teal, fontWeight: FontWeight.w600)),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              initialValue: _user.firstName,
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                              enabled: createProfile,
                              validator: (value){
                                return validateName(value);
                              },
                              onSaved: (value){
                                _user.firstName = value;
                              },
                              decoration: 
                              InputDecoration(
                                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                labelText: 'Имя',
                                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                                )
                              ),
                            SizedBox(height: 8),
                            TextFormField(
                              initialValue: _user.lastName,
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                              enabled: createProfile,
                              validator: (value){
                                return validateName(value);
                              },
                              onSaved: (value){
                                _user.lastName = value;
                              },
                              decoration: 
                                InputDecoration(
                                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                  labelText: 'Фамилия',
                                  labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                  contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                                )
                            ),
                            SizedBox(height: 8),
                            TextFormField(
                              initialValue: _user.email,
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                              enabled: createProfile,
                              keyboardType: TextInputType.emailAddress,
                              validator: (value){
                                return validateEmail(value);
                              },
                              onSaved: (value){
                                _user.email = value;
                              },
                              decoration: 
                                InputDecoration(
                                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                  labelText: 'Email',
                                  labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                  contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                                )
                            ),
                            SizedBox(height: 20),
                            createProfile
                            ? Container(
                              //width: 200,
                              height: 40,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                RaisedButton(
                                  elevation: 0,
                                  color: Colors.teal,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  onPressed: (){
                                    setState(() {
                                      if(_formKeyProfile.currentState.validate()){
                                        setUserInformation();
                                        createProfile = false;
                                      }
                                    });
                                  },
                                  child: Text('Сохранить изменения',style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600))
                                ),
                                SizedBox(width: 8),
                                Container(
                                 // width: 200,
                                  height: 40,
                                  child:  RaisedButton(
                                    color: Colors.red,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ),
                                    onPressed: (){
                                      setState(() {
                                        _getUserInformation();
                                        createProfile = false;
                                      });
                                    },
                                    child: Text('Отмена', style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600))
                                  ),
                                ),
                              ],)
                            )
                            : Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width*0.5,
                              child:  RaisedButton(
                                elevation: 0,
                                color: Colors.teal,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                onPressed:(){
                                  setState(() {
                                  createProfile = true;
                                });
                                },
                                child: Text('Редактировать',style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600)),
                              )
                            ),
                          ],
                        )
                      ),
                    ],
                  ),
                )
              ),
            ),
            Center(child: Form(
              key:_formKeyPassword,
              child:Column(
                children: <Widget>[
                  SizedBox(height: 20.0,),
                  Divider(color: primaryColor),
                  SizedBox(height: 10.0,),
                  Text('Изменение пароля', style: TextStyle(fontSize: 16, color: Colors.teal, fontWeight: FontWeight.w600)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                    child: 
                      Column(
                        children: <Widget>[
                          TextFormField(
                            initialValue: dataPassword.oldPassword,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                            obscureText: true,
                            validator: (value){
                              return validatePassword(value);
                            },
                            onSaved: (value){
                              dataPassword.oldPassword = value;
                            },
                            decoration: 
                              InputDecoration(
                                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                labelText: 'Старый пароль',
                                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                              )
                          ),
                          SizedBox(height: 8),
                          TextFormField(
                            obscureText: true,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                            initialValue: dataPassword.newPassword,
                            validator: (value){
                              return validatePassword(value);
                            },
                            onSaved: (value){
                              dataPassword.newPassword = value;
                            },
                            decoration: 
                              InputDecoration(
                                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                labelText: 'Новый пароль',
                                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                              )
                          ),
                          SizedBox(height: 8.0),
                          TextFormField(
                            obscureText: true,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                            initialValue: dataPassword.newPasswordClone,
                            validator: (value){
                              return validatePassword(value);
                            },
                            onSaved: (value){
                              dataPassword.newPasswordClone = value;
                            },
                            decoration: 
                              InputDecoration(
                                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                                labelText: 'Новый пароль ещё раз',
                                labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                                contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                              )
                          ),
                          SizedBox(height: 10),
                          successSetPassword? 
                          Container(child: Text('Пароль успешно изменён', style: TextStyle(fontSize: 16.0, color: Colors.green, fontWeight: FontWeight.w400),)) 
                          : error?
                          Container(child: Text('Ошибка смены пароля', style: TextStyle(fontSize: 16.0, color: Colors.redAccent, fontWeight: FontWeight.w400),),) 
                          : Container(),
                          SizedBox(height: 10,),
                          Center(
                            child:
                            Container(
                              width: MediaQuery.of(context).size.width*0.5,
                              height: 40,
                              child: RaisedButton(
                                elevation: 0,
                                color: Colors.teal,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                onPressed: (){
                                  setState((){
                                    if(_formKeyPassword.currentState.validate()){
                                      if(dataPassword.newPassword == dataPassword.newPasswordClone){
                                        _setPassword();
                                      }else{
                                          Scaffold.of(context).showSnackBar(SnackBar(
                                          content: Text('Пароли не совпадают', style: TextStyle(color: Colors.red)),
                                        ));
                                      }
                                    }
                                  });
                                },
                                child: Text('Сохранить изменения',style:TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.w600))
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    )
                ],
              )
            ),
            )
          ],
        )
      );
    }else{
      return LoadingProgressIndicator();
    }
  }

  @override
  initState(){
    _getUserInformation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _buildScreen(),
    );
  }
}