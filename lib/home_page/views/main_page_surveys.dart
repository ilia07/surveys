import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/core/AppConfig.dart';
import 'package:http/http.dart' as http;
import 'package:surveys/core/ui/LoadingProgressIndicator.dart';
import 'package:surveys/data/surveys_data.dart';
import 'package:surveys/home_page/views/page_survey.dart/page_survey.dart';
import 'package:surveys/login_page/login_page.dart';


class Surveys extends StatefulWidget {
  @override
  _SurveysState createState() => _SurveysState();
  }
  
  class _SurveysState extends State<Surveys> {
  AppConfig _appConfig = AppConfig();
  bool error = false;
  SurveysList _surveysList;
  bool nullSurveys = false;
  String searchString = '';
  final _searchKey = GlobalKey<FormState>();
  ScrollController _surveyListController = ScrollController();
   bool _endReached = false;
   int currentPage = 1, lastPage = 10;
   int page = 1;

   void scrollListener() {
    if (_surveyListController.offset >=
            _surveyListController.position.maxScrollExtent &&
        !_surveyListController.position.outOfRange) {
      setState(() {
        if (currentPage <= lastPage && !_endReached) {
          if (currentPage == lastPage) {
            _endReached = true;
          }
          currentPage = currentPage + 1;
        }
      });
      if (!_endReached) {
        page++;
        _getSurveys(page);
      }
    }
  }

  void _getSurveys(int page) async{
      final prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token');
      String apiRoot = await _appConfig.getApiRootUrl();
      try{
        var response = await http.get('$apiRoot/v1/surveys?select=assigned&searchString=$searchString&page=$page&size=10', headers: {"Content-Type": "application/json","Authorization": "Bearer $token"});
        if(response.statusCode == 200){
          setState(() {
            _surveysList = SurveysList.fromJson(json.decode(response.body));
            if(_surveysList.totalCount == 0){
              nullSurveys = true;
            }else{
              nullSurveys = false;
            }
          });
        }if(response.statusCode == 404){
          setState(() {
           error = true; 
          });
        }
        if(response.statusCode == 401){
        Navigator.of(context).pushNamed(LoginPage.tag);
      }
      }catch(e){
        print(e);
      }
  }

  void saveId(String id) async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('id', id );
  }

  Widget buildSurvey(Survey survey){
    return
      GestureDetector(
        onTap: (){
          saveId(survey.id);
          Navigator.push(context, MaterialPageRoute(
            builder: (context){
              return PageSurveys();
            }
          ));
        } ,
        child:
          Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10,),
              child:
                Card(
                  color: Colors.white,
                  elevation: 0,
                   shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: primaryColor, width: 0.8)
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
                        decoration: BoxDecoration(borderRadius: BorderRadius.vertical(top: Radius.circular(5)), color: primaryColor),
                        child: 
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text('${survey.title}',style: TextStyle(fontSize: 14,color: Colors.white,fontWeight: FontWeight.w600 ), textAlign: TextAlign.center,), 
                              )
                            ],  
                          ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text('Дата: '+'${survey.date}',style: TextStyle(fontSize: 12, color: Colors.black87, fontWeight: FontWeight.w400),),
                          Text('Автор: '+'${survey.creator.email}',style: TextStyle(fontSize: 12, color: Colors.black87, fontWeight: FontWeight.w400)),
                        ],
                      ),
                      SizedBox(height: 10.0),
                    ],
                  )
                ),
            ),
          ],
        )
      );
  }

  Widget _buildPage(){
    if(_surveysList != null){
      return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: <Widget>[
            SizedBox(height: 5,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Form(
                key: _searchKey,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width*0.62,
                      child: TextFormField(
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                        onSaved: (String value){
                          searchString = value;
                        },
                        decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5)),borderSide: BorderSide(color: primaryColor, width: 1.8)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5)),borderSide: BorderSide(color: primaryColor, width: 1)),
                        contentPadding: EdgeInsets.all(15),
                        hintText: 'Поиск по названию опроса',
                        hintStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Container(
                      height: 49,
                      width: MediaQuery.of(context).size.width*0.3,
                      child: RaisedButton(
                        color: Colors.teal,
                        elevation: 0,
                        child: Text('Поиск',style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.w600)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(5),bottomRight: Radius.circular(5)),
                        ),
                        onPressed: (){
                          _searchKey.currentState.save();
                          _getSurveys(page);
                        },
                      ),
                    ),
                  ],
              ),
              )
            ),
            nullSurveys? Column(
              children: <Widget>[
                SizedBox(height: 20),
                Text('Доступных опросов нет', style:TextStyle(fontSize: 14,), textAlign: TextAlign.center, )
              ],
            ) :
            Column(
              children: List.generate(_surveysList.totalCount,(int index){
                return buildSurvey(_surveysList.surveys[index]);
              })
            ),
            error? Text('Ошибка входных данных', style: TextStyle(fontSize: 14),) : Container(),
          ]
        )
      );
    }else{
       return LoadingProgressIndicator();
    }
  }

  @override
  initState(){
    _getSurveys(page);
    _surveyListController.addListener(scrollListener);
    super.initState();
  }
  
  @override
  void dispose(){
    _surveyListController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage();
  }
}