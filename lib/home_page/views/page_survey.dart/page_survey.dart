import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveys/Theme.dart';
import 'package:surveys/core/AppConfig.dart';
import 'package:http/http.dart' as http;
import 'package:surveys/core/ui/LoadingProgressIndicator.dart';
import 'package:surveys/core/utils/formaterMaxAnswers.dart';
import 'package:surveys/data/surveys_data.dart';
import 'dart:convert';
import 'package:surveys/login_page/login_page.dart';

class PageSurveys extends StatefulWidget {
  @override
  _PageSurveysState createState() => _PageSurveysState();
}

class AnswerServer{
  String idQuestion;
  List<String> answerList;
  String comment;
  AnswerServer(this.idQuestion,this.answerList,this.comment);
}

class _PageSurveysState extends State<PageSurveys> {
  AppConfig _appConfig = AppConfig();
  String idSurvey;
  bool isOpened;
  String text;
  SurveyFull survey;
  List<String> radioListAnswer;
  List<bool> checkBoxListAnswer;
  bool needCreateList = true;
  List <AnswerServer> answerServer = [];
  List <String> fieldList = [];
  String comment;
  String answerField;
  String field;
  final _formkey = GlobalKey<FormState>();
  bool error = false;
  String errorSrc = '';
  bool sending = false;

  void _getSurveys() async{
    final prefs = await SharedPreferences.getInstance();
    idSurvey = prefs.getString('id');
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    try{
      var response = await http.get('$apiRoot/v1/surveys/$idSurvey?full=true', headers: {"Authorization": "Bearer $token"});
      print(response.body);
      if(response.statusCode == 200){
        setState(() {
          survey = SurveyFull.fromJson(json.decode(response.body));
          radioListAnswer = List.generate(survey.qustions.length, ((int index) => ''));
        });
      }
      if(response.statusCode == 404){

      } 
      if(response.statusCode == 401){
        Navigator.of(context).pushNamed(LoginPage.tag);
      }
    }catch(e){
      print(e);
    }
  }

  void _postAnswer() async{
    final prefs = await SharedPreferences.getInstance();
    String surveyId = prefs.getString('id');
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    try{
      var resBody = {};
      var questions = [];
      int j = 0;
      for(int i=0; i< answerServer.length;i++){
        var answers = [];
        for(int y=0; y< answerServer[i].answerList.length; y++){
          var answer = {};
          if(answerServer[i].answerList[y] == 'field'){
            answer["value"] = fieldList[j];
            j++;
          }else{
            if(answerServer[i].answerList[y] == 'Opened'){
              answer['value'] = fieldList[j];
              j++;
            }else{
              answer["answerId"] = answerServer[i].answerList[y];
            }
          }
          answers.add(answer);
        }
        var question = {};
        question["questionId"] = answerServer[i].idQuestion;
        question["answers"] = answers;
        question["comment"] = answerServer[i].comment;
        questions.add(question);
      }
      resBody["questions"] = questions;
      var body = jsonEncode(resBody);
      print(body);
      var response = await http.post('$apiRoot/v1/Surveys/$surveyId/Forms', headers: {"Authorization": "Bearer $token", "Content-Type": "application/json"}, body: body );
      if(response.statusCode == 200 ){
        sending = true;
      }else{
        sending = false;
      }
    }catch(e){
    }
  }

  Widget inputForm(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(children: <Widget>[
        Text('Комментарий',style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
        SizedBox(height: 10,),
        TextFormField(
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 0.5)),
            focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 1.8)),
          ),
          enabled: true,
          maxLines: 2,
          onSaved: (value){
            setState(() {
              comment = value; 
            });
          },
        )
      ],
    ),);
  }
  
  Widget buildPageSuccess(){
    return Center(child:
      Column(
        children: <Widget>[
        Text('Спасибо за прохождение опроса',style:TextStyle(fontSize: 16, color: primaryColor, fontWeight: FontWeight.w600)),
        SizedBox(height: 10),
        Text('Ваша анкета успешно сохранена', style:TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
        SizedBox(height: 20,),
        Container(
          width: MediaQuery.of(context).size.width*0.7,
          height: 55,
          child:  RaisedButton(
            padding: EdgeInsets.all(8),
            child: Text('Отправить и перейти на страницу опросов',style: TextStyle(fontSize: 14, color:Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
            color: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            onPressed: ((){
              setState(() {
                _postAnswer();
                Navigator.pop(context, MaterialPageRoute(
                  builder: (context){
                    return PageSurveys();
                  }
                ));
              });
            }),
          ),
        ),
      ],),
    );
  }

  Widget buildRadio(Answer answer){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: primaryColor,
          value: '${answer.id}',
          groupValue: radioListAnswer[indexQ],
          onChanged: (value){
          setState(() {
            radioListAnswer[indexQ] = value;
          });
        }),
        Text('${answer.title}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
      ],
    );
  }

  Widget buildCheckbox(Answer answer, int index){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          activeColor: primaryColor,
          value: checkBoxListAnswer[index],
          onChanged: (bool value){
            setState(() {
              checkBoxListAnswer[index] = value;
            });
          },
        ),
        Text('${answer.title}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
      ],
    );
  }

   Widget buildListAnswers(Question question){
    if(isOpened == true){
      return TextFormField(
        onSaved: (val){
          text = val;
        },
        maxLines: 2,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 0.5)),
          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 1.8)),
          hintText: 'Другое',
          hintStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        ),
      );
    }
    if(question.maxAnswers == 1){
      return Column(
        children: <Widget>[
          Column(
            children: List.generate(question.answers.length, (int index) =>
            buildRadio(question.answers[index])),
          ), 
          question.isField? question.isComment? inputForm(): Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
            Radio(
              value: 'field',
              groupValue: radioListAnswer[indexQ],
              onChanged: (index){
                setState(() {
                 radioListAnswer[indexQ] = index; 
                });
              },
            ),
            Text('${question.fieldTitle}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
            SizedBox(width: 10),
            Container(
              width: 240,
              child: TextFormField(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(7.0),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 0.5)),
                ),
                onSaved: (value){
                  field = value;
                },
              ),
            ),
          ],)  : Container(),
        ],
      );
    }else{
      return Column(
        children: <Widget>[
          Column(
            children: List.generate(question.answers.length, (int index) =>
            buildCheckbox(question.answers[index], index)),
          ),
           question.isField? question.isComment? inputForm(): Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
            Checkbox(
              value: checkBoxListAnswer.last,
              onChanged: (bool value){
                setState(() {
                  checkBoxListAnswer.last = value;
                });
              },
            ),
            Text('${question.fieldTitle}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
            SizedBox(width: 10,),
            Container(
              width: MediaQuery.of(context).size.width*0.5,
              child: TextFormField(
                initialValue: '',
                style: TextStyle(fontSize: 14),
                 decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(7.0),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 0.5)),
                ),
                onSaved: (value){
                  field = value;
                },
              ),
            ),
          ],)  : Container(),
        ],
      );
    }
  }

  Widget buildPageQuestion(Question question){
    if(needCreateList == true){
      if(question.isField == true){
        checkBoxListAnswer = List.generate(question.answers.length+1, (int index) => false);
      }else{
        if(question.type == 'Opened'){
          isOpened = true;
          text = '';
        }else{
          checkBoxListAnswer = List.generate(question.answers.length, (int index)=> false);
        }
      }
      needCreateList = false;
    }

    bool visibleMaxAnswers = false;
    if(question.maxAnswers > 1){
      visibleMaxAnswers = true;
    }

    bool temp2(){
      if(survey.qustions.length - 1 == indexQ){
        return true;
      }else{
        return false;
      }
    }

    return Column(
      children: <Widget>[
        SizedBox(height: 5),
        Text('${question.title}',style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
        SizedBox(height: 10,),
        visibleMaxAnswers? Text(formater(question.maxAnswers), style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, fontStyle: FontStyle.italic), textAlign: TextAlign.center,): Container(),
        visibleMaxAnswers? SizedBox(height: 10): Container(),
        Form(
          key: _formkey,
          child: buildListAnswers(question),
        ),
        error? Column(
          children: <Widget>[
            SizedBox(height: 8),
            Text('$errorSrc', style: TextStyle(fontSize: 14, color: Colors.redAccent, fontWeight: FontWeight.w600)),
            SizedBox(height: 8),
          ],
        ) : Container(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
          child:  endQuestion? Container() : RaisedButton(
            padding: EdgeInsets.all(10),
            child: temp2()? Text('Завершить прохождение',style: TextStyle(fontSize: 14, color:Colors.white, fontWeight: FontWeight.w600)):Text('Следующий вопрос',style: TextStyle(fontSize: 14, color:Colors.white, fontWeight: FontWeight.w600)),
            color: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
            onPressed: ((){
              setState(() {
                _formkey.currentState.save();
                error = false;
                List <String> temp = [];
                needCreateList = true;
                if(isOpened == true){
                  if(text == '' || text == null){
                    error = true;
                    errorSrc = 'Заполните поле';
                  }else{
                    error = false;
                  }
                  if(error == false){
                    temp.add('Opened');
                    fieldList.add(text);
                    isOpened = false;
                    answerServer.add(AnswerServer(question.id,temp,comment));
                    indexQ++;
                  }
                  _formkey.currentState.reset();
                  if(indexQ >= survey.qustions.length){
                    endQuestion = true;
                  }
                }else{
                  if(question.maxAnswers > 1){
                    for(int i=0; i < question.answers.length; i++){
                      if(checkBoxListAnswer[i] == true){
                        temp.add(question.answers[i].id);
                      }
                    }
                    if(question.isField == true && checkBoxListAnswer.last == true){
                      if(field == null|| field == ''){
                        error = true;
                        errorSrc = 'Пожалуйста дайте ответ';
                      }else{
                        temp.add('field');
                        fieldList.add(field);
                        field = '';
                      }
                    }
                    if(temp.length > question.maxAnswers){
                      errorSrc = 'Выбрано слишком много ответов';
                      error = true;
                    }
                    if(temp.length == 0){
                      errorSrc = 'Пожалуйста, дайте ответ на вопрос';
                      error = true;
                    }
                  }else{
                    if(radioListAnswer[indexQ] == 'field'){
                      if(field == ''){
                        error = true;
                        errorSrc = 'Пожалуйста дайте ответ';
                      }else{
                        fieldList.add(field);
                        field = '';
                      }
                    }
                    if(radioListAnswer[indexQ] == ''){
                      errorSrc = 'Пожалуйста, ответьте на вопрос';
                      error = true;
                    }else{
                      temp.add(radioListAnswer[indexQ]);
                    }
                    if(temp.length == 0){
                      error = true;
                    }
                  }
                  if(error == false){
                    if(comment == ''|| comment == null){
                      answerServer.add(AnswerServer(question.id,temp,''));
                    }else{
                      answerServer.add(AnswerServer(question.id,temp,comment));
                      comment = '';
                    }
                    indexQ++;
                  }
                  _formkey.currentState.reset();
                  if(indexQ >= survey.qustions.length){
                    endQuestion = true;
                  }
                }
              });
            }),
          ),
        ),
      ],
    );
  }

  bool endQuestion = false;
  bool started = false;
  int indexQ = 0;
  bool visibleBackButton = true;

  @override
  initState(){
    super.initState();
    _getSurveys();
  }

  @override
  Widget build(BuildContext context) {
    if(survey != null){
      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: primaryColor,
          title: Text('Прохождение опроса', style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.w600)),
          automaticallyImplyLeading: visibleBackButton,
        ),
        backgroundColor: Colors.white,
        body: 
        started?
          Center(
            child:
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child:
                ListView(
                shrinkWrap: endQuestion,
                children: <Widget>[
                  SizedBox(height: 10),
                  endQuestion? 
                    buildPageSuccess()
                  : buildPageQuestion(survey.qustions[indexQ]),
                  SizedBox(height: 10),
                ],
              )
            ),
          )
        : Center(
            child: 
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Text('${survey.title}', style: TextStyle(fontSize: 16, color: primaryColor, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
                    Column(
                      children: <Widget>[
                        SizedBox(height: 10.0),
                        Text('Автор опроса: '+'${survey.creator.email}',style: TextStyle(fontSize: 14, color: Colors.black87, fontWeight: FontWeight.w400)),
                        SizedBox(height: 10.0),
                        Text('${survey.date}', style: TextStyle(fontSize: 12, color :Colors.black54, fontWeight: FontWeight.w300, fontStyle: FontStyle.italic)),
                        SizedBox(height: 10),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        RaisedButton(
                          child: Text('Начать прохождение опроса', style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.w600)),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          color: primaryColor,
                          onPressed: (){
                            setState(() {
                            started = true; 
                            visibleBackButton = false;
                            });
                          },
                        ),
                      ],
                    ),
                  ],
                )
              ),
        )
      );
    }else{
      return LoadingProgressIndicator();
    }
  }
}