import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:surveys/Theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveys/core/AppConfig.dart';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:surveys/core/utils/showSnackBar.dart';
import 'package:surveys/core/utils/validate.dart';
import 'package:surveys/data/feedback.dart';
import 'package:surveys/login_page/login_page.dart';

class AboutAppPage extends StatefulWidget {
  @override
  _AboutAppPageState createState() => _AboutAppPageState();
}

class _AboutAppPageState extends State<AboutAppPage> {
  final _formKey = GlobalKey<FormState>();
  AppConfig _appConfig = AppConfig();
  var dio = Dio();
  FeedbackData model = FeedbackData();
  String title;
  String text;

  void sendForm() async{
    _formKey.currentState.save();
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String apiRoot = await _appConfig.getApiRootUrl();
    try{
      model.model = json.encode({'title': title, 'text' : text});
      Response response = await dio.post('$apiRoot/v1/feedback', options: Options(headers: {"Authorization":"Bearer $token"}), data: model.getFormDataValue());
      if(response.statusCode == 200){
        setState((){
          showSnackbar(context, 'Сообщение успешно отправлено');
          _formKey.currentState.reset();
        });
      }
      if(response.statusCode == 401){
        Navigator.of(context).pushNamed(LoginPage.tag);
      }
    }catch(e){
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(height: 50,),
            Text('Проект Surveys', style:TextStyle(fontSize: 18.0, color: primaryColor, fontWeight: FontWeight.w600)),
            SizedBox(height: 20,),
            Text('Это приложнение создано для прохождения опросов', style:TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),
            SizedBox(height: 10,),
            Text('Создано ГПО "Социальные опросы"', style: TextStyle(fontSize: 12, color: primaryColor, fontWeight: FontWeight.w400, fontStyle: FontStyle.italic)),
          ],
        ),
        Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20,),
              Divider(color: primaryColor,),
              SizedBox(height: 20,),
              Text('Обратная связь', style: TextStyle(fontSize: 16, color: primaryColor, fontWeight: FontWeight.w600)),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                      validator: (value){
                        return validateEmpty(value);
                      },
                      onSaved: (value){
                        title = value;
                      },
                      decoration: 
                      InputDecoration(
                        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor.withOpacity(0.5))),
                        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor, width: 2)),
                        labelText: 'Заголовок',
                        labelStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                        contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                      )
                    ),
                    SizedBox(height: 30,),
                    Row(
                      children: <Widget>[
                        Text('Сообщение', style: TextStyle(fontSize: 14, color: primaryColor, fontWeight: FontWeight.w600),),
                      ],
                    ),
                    SizedBox(height: 10,),
                    TextFormField(
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                      validator: (value){
                        return validateFeedbackText(value);
                      },
                      onSaved: (value){
                        text = value;
                      },
                      maxLines: 4,
                      keyboardType: TextInputType.text,
                      maxLength: 100,
                      decoration: 
                      InputDecoration(
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 0.5)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5),borderSide: BorderSide(color: primaryColor, width: 1.8)),
                        hintText: 'Сообщение',
                        hintStyle: TextStyle(color: Colors.black54, fontSize: 14.0, fontWeight: FontWeight.w400),
                        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                      )
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width*0.5,
                          child: RaisedButton(
                            color: primaryColor,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: Text('Отправить', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),),
                            onPressed: (){
                              if(_formKey.currentState.validate()){
                                sendForm();
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}