import 'package:flutter/material.dart';
import 'package:surveys/home_page/home_page.dart';
import 'package:surveys/login_page/login_page.dart';
import 'package:surveys/login_page/main_login_page.dart';
import 'package:surveys/login_page/registration_page.dart';
import 'package:surveys/login_page/ressetpassword_page.dart';
import 'package:surveys/login_page/success-page.dart';

void main(){
  runApp(SurveysApp());
}

class SurveysApp extends StatelessWidget {
  final routes = <String,WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
    RessetPasswordPage.tag: (context) => RessetPasswordPage(),
    MainLoginPage.tag: (context) => MainLoginPage(),
    RegistrationPage.tag: (context) => RegistrationPage(),
    SuccessPage.tag: (context) => SuccessPage(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Surveys',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
        fontFamily: 'Open_Sans'
      ),
      home: MainLoginPage(),
      routes: routes,
    );
  }
}
